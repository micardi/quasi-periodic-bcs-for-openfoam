# README #

This repository contains the OpenFOAM libraries for the quasi-periodic boundary conditions developed to study the upscaling of particle transport in periodic porous media

Boccardo, G., Crevacore, E., Sethi, R., & Icardi, M. (2017). A robust upscaling of the effective particle deposition rate in porous media. arXiv preprint arXiv:1702.04527.

The code will be entirely uploaded soon...

### Developers ###

* Matteo Icardi (University of Warwick)